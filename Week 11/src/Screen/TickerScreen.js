import { Container, ticker, Graphics } from "pixi.js";

var Config = require('Config');

class TickerScreen extends Container{
    constructor(){
        super();

        this.graphicVelocity = {
            x: 50,
            y: 50
        };
        this.direction = {
            x:1,
            y:1
        };

        this.timeElapsed = 1;

        this.ticker = ticker.shared;

/*         this.bindedUpdate = this.update.bind(this);

        this.ticker.add(this.bindedUpdate);
 */
        this.ticker.add(this.update, this);

        this.graphics = new Graphics();
        this.graphics.beginFill(0xaabb99);
        this.graphics.drawRect(-25,-25,50,50);
        this.graphics.endFill();

        this.graphics.x = 100;
        this.graphics.y = 100;
        this.addChild(this.graphics);

        this.timeStart = 30;
        this.text = new PIXI.Text(this.timeStart, {
            fill: 0xffffff,
            fontSize: 50
        });
        this.addChild(this.text);

        this.star = new Graphics();
        this.star.beginFill(0xffff00);
        this.star.drawStar(0,0,5,30,10,0);
        this.star.endFill();
        this.addChild(this.star);
        this.star.x = 20;
        this.star.y = 20;
    }

    update(e){
        var dts = this.ticker.elapsedMS * 0.001;

        this.graphics.rotation += dts;
        if(this.timeStart > 0)  this.timeStart -= dts;
        if(this.timeStart <0) this.timeStart = 0;
        this.text.text = Math.ceil(this.timeStart);

        this.timeElapsed -= dts;
        console.log(this.timeElapsed);
        if(this.timeElapsed<=0){
            let shapes = new Graphics();
            let x = Math.random() * (Config.BUILD.WIDTH-30) + 30;
            let y = Math.random() * (Config.BUILD.HEIGHT-30) + 30;
            shapes.beginFill(0x888888);
            shapes.drawRect(x,y,30,30);
            shapes.endFill();
            this.addChild(shapes);

            this.star.x = Math.random() * (Config.BUILD.WIDTH-20) + 20;
            this.star.y = Math.random() * (Config.BUILD.HEIGHT-20) + 20;
            this.star.y = this.star.x >= Config.BUILD.HEIGHT ? Math.random() * (Config.BUILD.HEIGHT-20) + 20 : this.star.x;

            this.timeElapsed = 1;
        }


        //console.log(this.direction);
        //console.log("x: " + this.graphics.x + " " + "y: " + this.graphics.y);
        {
        // if(this.direction.x == 1) this.graphics.x += this.graphicVelocity.x * dts;
        // else if(this.direction.x == -1) this.graphics.x -= this.graphicVelocity.x * dts;

        // if(this.direction.y == 1) this.graphics.y += this.graphicVelocity.y * dts;
        // else if(this.direction.y == -1) this.graphics.y -= this.graphicVelocity.y * dts;

        // if(this.graphics.x <= 0) this.direction.x = 1;
        // else if (this.graphics.x >= Config.BUILD.WIDTH - 50) this.direction.x = -1;

        // if(this.graphics.y <= 0) this.direction.y = 1;
        // else if (this.graphics.y >= Config.BUILD.HEIGHT - 50) this.direction.y = -1;
        }
        /* console.log(this.timeElapsed);
        this.timeElapsed -= dts;
        if(this.timeElapsed <= 0) this.ticker.remove(this.bindedUpdate); */
    }
}

export default TickerScreen;